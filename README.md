# CRUX Docker Base Image

**Fork of [Crux Docker image repo by jaeger@](https://github.com/mhoush/crux-docker-image)**

## Branches:
* master and 3.2 are from the upstream repo
* 3.3 and 3.2-for-upstream are my changes

## Changes:
* Check if script is run as root.
* Download ISO and md5sum to the top directory, 
copy them to work directory as needed (re-downloading because I made a typo s*cks).
 
## TODO
* After release, official ISO doesn't change, but the updated ones, well, do.
  Find a way to check a timestamp or md5sum of the server ISO against the local one, 
  to decide whether to d/l it or not.
 
All the bugs are mine :)
FROM scratch
MAINTAINER jaeger@crux.ninja
LABEL name="CRUX 3.2 Base Image"

ADD crux-3.2-docker-base-image.tar.xz /

CMD [ "/bin/bash" ]
